﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Transport.Console;
using Transport.Domain;

namespace Transport.Viewer {
   /// <summary>
   /// Interaction logic for MainWindow.xaml
   /// </summary>
   public partial class MainWindow : Window {
      private readonly ConsoleWindow consoleWindow;
      private readonly Map map = Program.Transporter.Map;

      private MapWindow mapWindow;

      public MainWindow ( ) {
         InitializeComponent( );
         consoleWindow = new ConsoleWindow(TestBox);
         System.Console.SetOut(consoleWindow);

         mapWindow = new MapWindow(map);
         MainGrid.Children.Add(mapWindow);
         System.Console.WriteLine("MapWindow initialized");

         Start.Click += Start_Click;
         Program.Transporter.SetMatrix(mapWindow.MapMatrix);
      }

      void Start_Click (object sender, RoutedEventArgs e) {
         TestBox.Clear( );
         Program.Transporter.Start( );
      }

   }
}
