﻿using System;
using System.Linq;
using System.Text;
using System.Windows;
using System.Collections.Generic;

using Transport.Domain;
using System.Windows.Media;
using System.Windows.Shapes;

namespace Transport.Viewer {
   internal class MapMatrix : IMapMatrix {

      internal double Width { get; private set; }
      internal double Height { get; private set; }
      internal double RowInterval { get; private set; }
      internal double ColumnInterval { get; private set; }

      public int[] Rows { get; private set; }
      public int[] Columns { get; private set; }
      public IEnumerable<Line> GridLines { get; private set; }


      internal MapMatrix (Size MapSize, Map CityMap) {
         Width = MapSize.Width;
         Height = MapSize.Height;

         Rows = Enumerable.Range(0, CityMap.Width).ToArray( );
         Columns = Enumerable.Range(0, CityMap.Length).ToArray( );

         RowInterval = Height / Rows.Length;
         ColumnInterval = Width / Columns.Length;

         DrawMatrix( );
      }


      #region IMapMatrix Members

      public event MatrixPopulated MatrixPopulated;

      #endregion


      private void DrawMatrix ( ) {
         var gridLines = new List<Line>( );

         foreach (var column in Columns.Skip(1)) {
            var gridLine = new Line( ) {
               X1 = ColumnInterval * column,
               X2 = ColumnInterval * column,
               Y1 = 0,
               Y2 = Height,
               Stroke = Brushes.Aquamarine,
               StrokeThickness = 1,
               SnapsToDevicePixels = true
            };
            gridLine.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);

            gridLines.Add(gridLine);
         }

         foreach (var row in Rows.Skip(1)) {
            var gridLine = new Line( ) {
               X1 = 0,
               X2 = Width,
               Y1 = RowInterval * row,
               Y2 = RowInterval * row,
               Stroke = Brushes.Aquamarine,
               StrokeThickness = 1,
               SnapsToDevicePixels = true
            };
            gridLine.SetValue(RenderOptions.EdgeModeProperty, EdgeMode.Aliased);

            gridLines.Add(gridLine);
         }

         GridLines = gridLines;
      }

   }
}
