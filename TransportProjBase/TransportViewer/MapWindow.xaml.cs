﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Transport.Domain;

namespace Transport.Viewer {

   /// <summary>
   /// Interaction logic for MapWindow.xaml
   /// </summary>
   public partial class MapWindow : UserControl {
      private MapMatrix mapMatrix;

      public IMapMatrix MapMatrix {
         get { return mapMatrix; }
      }
      public Map CityMap { get; private set; }

      public MapWindow (Map Map) {
         InitializeComponent( );
         CityMap = Map;

         Width = 500;
         Height = 500;
         mapMatrix = new MapMatrix(new System.Windows.Size((double)Width, (double)Height), CityMap);
         DrawGrid( );

      }

      private void DrawGrid ( ) {
         foreach (var gridLine in mapMatrix.GridLines) {
            MapCanvas.Children.Add(gridLine);
         }
      }

      internal Bitmap RenderImage ( ) {
         Rect bounds = VisualTreeHelper.GetDescendantBounds(this);
         int dpi = 96;

         this.Measure(new System.Windows.Size(Width, Height));
         this.Arrange(new System.Windows.Rect(new System.Windows.Size(Width, Height)));

         RenderTargetBitmap renderer = new RenderTargetBitmap(
            Convert.ToInt32(Width),
            Convert.ToInt32(Height),
            dpi,
            dpi,
            PixelFormats.Pbgra32);

         //DrawingVisual dv = new DrawingVisual( );

         //using (DrawingContext dc = dv.RenderOpen( )) {
         //   VisualBrush vb = new VisualBrush(MapCanvas);
         //   dc.DrawRectangle(vb, null, new Rect(new System.Windows.Point( ), bounds.Size));
         //}

         renderer.Render(this);

         var bitMap = default(Bitmap);
         PngBitmapEncoder png = new PngBitmapEncoder( );
         png.Frames.Add(BitmapFrame.Create(renderer));
         using (var stream = new MemoryStream()) {
            png.Save(stream);
            bitMap = new Bitmap(stream);
         }

         return bitMap;

      }
   }
}
