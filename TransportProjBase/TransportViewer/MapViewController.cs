﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows;

using Transport.Console;

namespace Transport.Viewer {
   public class MapViewController {
      private static readonly Lazy<MapViewController> instance = new Lazy<MapViewController>(( ) => new MapViewController( ));
      private static readonly Lazy<Program> program = new Lazy<Program>(( ) => Program.Transporter);

      private readonly Program transporter;
      private readonly System.Windows.Size windowSize;
      private readonly MapMatrix mapMatrix;


      public static MapViewController Default {
         get { return instance.Value; }
      }

      public int MapInterval { get; private set; }


      private MapViewController ( ) {
         transporter = program.Value;
         windowSize = new System.Windows.Size( ) {
            Width = 500,
            Height = 500
         };

         mapMatrix = new MapMatrix(windowSize, transporter.Map);
         MapInterval = (int)mapMatrix.RowInterval;
         transporter.SetMatrix(mapMatrix);
      }


      public Bitmap GetMapImage ( ) {
         MapWindow window = new MapWindow(transporter.Map);
         return window.RenderImage( );
      }

   }
}
