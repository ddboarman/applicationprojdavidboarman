﻿using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
//
using Transport.Domain;
using Transport.Backend;

namespace Transport.Console {
   public class Program {
      private static readonly int size = 25;
      private static readonly City MyCity = new City(size, size);

      public static readonly Program Transporter = new Program( );


      private IMapMatrix matrix;

      public Map Map {
         get { return MyCity.Map; }
      }

      public void SetMatrix (IMapMatrix Matrix) {
         matrix = Matrix;
      }

      private Program ( ) {
         Random rand = new Random( );
         int CityLength = MyCity.Map.Length;
         int CityWidth = MyCity.Map.Width;

         Navigator.SetCity(MyCity);

         Car sedan = MyCity.AddCarToCity(new Sedan(rand.Next(CityLength - 1), rand.Next(CityWidth - 1), MyCity, null));
         Car mach5 = MyCity.AddCarToCity(new MachFive(rand.Next(CityLength - 1), rand.Next(CityWidth - 1), MyCity, null));
      }

      static void Main (string[] args) {
         Transporter.Start( );

         return;
      }

      public void Start ( ) {
         // running console program
         if (Transporter.matrix == null) {
            var key = default(int);
            while ((key = System.Console.Read( )) != 'q') {
               if (key == 'r') {
                  RunConsole( );
               }
            }

            return;
         }

         // else either WPF or web front end
         Task runConsole = new Task(( ) => RunConsole( ));
         runConsole.Start( );
      }

      private static void RunConsole ( ) {
         Random rand = new Random( );
         int CityLength = MyCity.Map.Length;
         int CityWidth = MyCity.Map.Width;

         Passenger passenger = MyCity.AddPassengerToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1), rand.Next(CityLength - 1), rand.Next(CityWidth - 1));
         Car car = MyCity.RegisteredCars
            .Where(c => c.GetType( ).Name == "MachFive")
            .First( );

         while (!passenger.IsAtDestination( )) {
            Thread.Sleep(1200);   // slowed output for reading
            Tick(car, passenger);
         }

         passenger.GetOutOfCar( );

         return;
      }

      /// <summary>
      /// Takes one action (move the car one spot or pick up the passenger).
      /// </summary>
      /// <param name="car">The car to move</param>
      /// <param name="passenger">The passenger to pick up</param>
      private static void Tick (Car car, Passenger passenger) {
         if (passenger.Car == null && 
            car.Location.XPos == passenger.GetCurrentXPos( ) && 
            car.Location.YPos == passenger.GetCurrentYPos( )) {
            passenger.GetInCar(car);

            return;
         }
         else if (car.Passenger == null) {
            // this is command sets a destination; 
            // not the same as Passenger.GetInCar
            car.PickupPassenger(passenger);
         }

         car.MoveNext();
      }

   }
}
