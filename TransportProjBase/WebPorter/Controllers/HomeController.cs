﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Net;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Collections.Concurrent;
//
using Transport.WebPorter.Models;
//
using Transport.Domain;
using Transport.Viewer;
using Transport.Backend;
using Transport.Console;

namespace Transport.WebPorter.Controllers {
   public class HomeController : Controller {

      private static readonly int defaultPort = 50500;

      private static MapViewController mapVuController = GetMapViewController( );
      private static Dictionary<long, ClientModel> unregisteredClients = new Dictionary<long, ClientModel>( );
      private static IDictionary<IPAddress, ClientModel> webClients = new Dictionary<IPAddress, ClientModel>( );

      private static readonly ConcurrentQueue<UpdatePacket> updatePackets = new ConcurrentQueue<UpdatePacket>( );

      public ActionResult Index ( ) {
         var client = new ClientModel(DateTime.Now.Ticks);
         unregisteredClients.Add(client.Id, client);

         return View(client);
      }

      public ActionResult About ( ) {
         ViewBag.Message = "Your application description page.";

         return View( );
      }

      public ActionResult Contact ( ) {
         ViewBag.Message = "Your contact page.";

         return View( );
      }

      public ActionResult RegisterClient ( ) {
         var ipAddress = IPAddress.Parse(Request.QueryString["ip"].ToString( ));
         var id = long.Parse(Request.QueryString["id"].ToString( ));

         var client = default(ClientModel);
         if (!unregisteredClients.TryGetValue(id, out client)) {
            client = new ClientModel(id);
         }
         else {
            unregisteredClients.Remove(id);
         }

         client.IpAddress = ipAddress;
         client.Port = defaultPort;

         if (!webClients.ContainsKey(ipAddress)) {
            webClients.Add(ipAddress, client);
         }

         return Json(new {
            socketport = client.Port
         }, JsonRequestBehavior.AllowGet);
      }

      public ActionResult GetMapImage ( ) {
         var ipAddress = IPAddress.Parse(Request.QueryString["ip"].ToString( ));
         var client = webClients[ipAddress];

         var mapImage = GetBitmap( );
         var fileName = string.Format("map_{0}_{1}_{2}.png",
            client.Id,
            mapImage.Width,
            mapImage.Height);
         var fileSpec = Path.Combine(Server.MapPath("~/Images/Maps"), fileName);
         mapImage.Save(fileSpec);

         return Json(new {
            map = new {
               imagefile = fileName,
               width = mapImage.Width,
               height = mapImage.Height,
               interval = mapVuController.MapInterval
            }
         }, JsonRequestBehavior.AllowGet);
      }

      public ActionResult StartSimulation ( ) {
         Program.Transporter.Start( );

         return Json(new {
            success = true
         }, JsonRequestBehavior.AllowGet);
      }

      public ActionResult GetMapUpdate ( ) {
         var ipAddress = IPAddress.Parse(Request.QueryString["ip"].ToString( ));
         var client = webClients[ipAddress];
         var updatePacket = default(UpdatePacket);

         updatePackets.TryDequeue(out updatePacket);

         if (updatePacket != null) {
            return Json(new {
               update = updatePacket.Info.Data
            }, JsonRequestBehavior.AllowGet);
         }
         else {
            return Json(new {
               update = default(UpdatePacket),
               city = new {
                  cars = GetRegisteredCarsJson( )
               }
            }, JsonRequestBehavior.AllowGet);
         }
      }


      private static void Navigator_CarMoved (Car Car, Direction Direction, Location OldLocation, Location NewLocation) {
         updatePackets.Enqueue(new UpdatePacket( ) {
            Info = new JsonResult( ) {
               Data = new {
                  message = string.Format("{0} moved {1} from:{2}   to:{3}",
                     Car.ID,
                     Direction,
                     OldLocation,
                     NewLocation),
                  car = new {
                     vin = Car.VIN,
                     id = Car.ID,
                     destination = GetPositionJson(Car.GetDestination( )),
                     direction = Direction,
                     oldlocation = GetPositionJson(OldLocation),
                     newlocation = GetPositionJson(NewLocation)
                  },
                  city = new {
                     cars = GetRegisteredCarsJson( )
                  }
               },
               JsonRequestBehavior = JsonRequestBehavior.AllowGet
            }
         });
      }

      private static object GetPositionJson (IMapLocation location) {
         return new {
            x = location.XPos,
            y = location.YPos
         };
      }

      private static object GetRegisteredCarsJson ( ) {
         return Navigator.City.RegisteredCars
            .Select(c => new {
               id = c.ID,
               location = new {
                  x = c.Location.XPos,
                  y = c.Location.YPos
               }
            })
            .ToArray( );
      }

      private static MapViewController GetMapViewController ( ) {
         Navigator.CarMoved += Navigator_CarMoved;

         var mvController = default(MapViewController);

         Thread thread = new Thread(( ) => {
            mvController = MapViewController.Default;
         });
         thread.SetApartmentState(ApartmentState.STA);
         thread.Start( );
         thread.Join( );

         return mvController;
      }

      private static Bitmap GetBitmap ( ) {
         var bitmap = default(Bitmap);

         Thread thread = new Thread(( ) => {
            bitmap = mapVuController.GetMapImage( );
         });
         thread.SetApartmentState(ApartmentState.STA);
         thread.Start( );
         thread.Join( );

         return bitmap;
      }

   }
}