﻿using System;
using System.Linq;
using System.Net;
using System.Text;
using System.Collections.Generic;

namespace Transport.WebPorter.Models {
   public class ClientModel {
      public long Id { get; private set; }
      public IPAddress IpAddress { get; set; }
      public int Port { get; set; }
      public string ImageFile { get; set; }


      public ClientModel (long ClientId) {
         Id = ClientId;
      }


      public override bool Equals (object obj) {
         var model = obj as ClientModel;
         if (model == null) {
            return false;
         }

         return Id == model.Id;
      }
   }
}
