﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Transport.WebPorter.Startup))]
namespace Transport.WebPorter
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
