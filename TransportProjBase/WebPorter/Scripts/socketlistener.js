﻿
function SocketListener() {

    this.getClientIp = function (registerDelegate) {
        $.get("http://ipinfo.io", function (response) {
            registerDelegate(response.ip);
        }, "jsonp");
    }

    this.setWebSocket = function (port) {
        if ('WebSocket' in window) {
            /* WebSocket is supported. You can proceed with your code*/
            console.log("WebSocket supported - setting up on port " + port);
        } else {
            /*WebSockets are not supported. Try a fallback method like long-polling etc*/
        }
    }
}