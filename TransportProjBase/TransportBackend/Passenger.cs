﻿using System;

namespace Transport.Backend {
   public class Passenger {
      
      public int StartingXPos { get; private set; }
      public int StartingYPos { get; private set; }
      public int DestinationXPos { get; private set; }
      public int DestinationYPos { get; private set; }
      public Car Car { get; set; }
      public City City { get; private set; }

      public Passenger (int startXPos, int startYPos, int destXPos, int destYPos, City city) {
         StartingXPos = startXPos;
         StartingYPos = startYPos;
         DestinationXPos = destXPos;
         DestinationYPos = destYPos;
         City = city;
      }

      public void GetInCar (Car car) {
         Car = car;
         Console.WriteLine("Passenger got in car.");
         car.AssignDestination(new Location(DestinationXPos, DestinationYPos));
      }

      public void GetOutOfCar ( ) {
         Car.DropOffPassenger(this);
         Car = null;
         Console.WriteLine("Passenger at destination.");
      }

      public int GetCurrentXPos ( ) {
         if (Car == null) {
            return StartingXPos;
         }
         else {
            return Car.Location.XPos;
         }
      }

      public int GetCurrentYPos ( ) {
         if (Car == null) {
            return StartingYPos;
         }
         else {
            return Car.Location.YPos;
         }
      }

      public bool IsAtDestination ( ) {
         return GetCurrentXPos( ) == DestinationXPos && 
            GetCurrentYPos( ) == DestinationYPos;
      }
   }
}
