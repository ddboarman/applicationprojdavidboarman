﻿using System;

using Transport.Domain;

namespace Transport.Backend {
   public static class Navigator {
      private static CityPosition cityPosition;

      public static void SetCity (City MyCity) {
         cityPosition = new CityPosition(MyCity);
         City = MyCity;
      }

      public static event CarMoved CarMoved;

      public static City City { get; private set; }

      public static bool AtDestination (this Car car, Destination destination) {
         return car.Location.XPos == destination.XPos && car.Location.YPos == destination.YPos;
      }

      public static Destination GetDestination (this Car car) {
         return new Destination(car);
      }

      public static int MoveNext (this Car car) {
         var distance = 0;
         var destination = new Destination(car);
         var direction = Direction.Unknown;
         var oldLocation = new Location(car.Location.XPos, car.Location.YPos);

         while (distance < car.Speed) {
            // if at destination then break
            if (car.AtDestination(destination)) {
               break;
            }

            // move north/south closer to destination
            if (car.Location.YPos > destination.YPos) {
               direction |= car.Move(Direction.South);
            }
            else if (car.Location.YPos < destination.YPos) {
               direction |= car.Move(Direction.North);
            }
            // move east/west closer to destination
            else if (car.Location.XPos > destination.XPos) {
               direction |= car.Move(Direction.West);
            }
            else if (car.Location.XPos < destination.XPos) {
               direction |= car.Move(Direction.East);
            }
            
            ++distance;
         }

         var newLocation = new Location(car.Location.XPos, car.Location.YPos);

         var handler = default(CarMoved);
         if ((handler = CarMoved) != null) {
            handler(car, direction, oldLocation, newLocation);
         }

         return distance;
      }



      public struct Destination : IMapLocation {
         private int xPos;
         private int yPos;


         public int XPos {
            get { return xPos; }
            set { xPos = value; }
         }

         public int YPos {
            get { return yPos; }
            set { yPos = value; }
         }


         internal Destination (Car car) {
            var destination = car.Passenger.Car == null ?
            new {
               XPos = car.Passenger.GetCurrentXPos( ),
               YPos = car.Passenger.GetCurrentYPos( )
            } :
            new {
               XPos = car.Passenger.DestinationXPos,
               YPos = car.Passenger.DestinationYPos
            };

            xPos = destination.XPos;
            yPos = destination.YPos;
         }

         public override string ToString ( ) {
            return string.Format("[{0}, {1}]", xPos, yPos);
         }
      }
   }

   public struct Location : IMapLocation {
      private readonly int xPos;
      private readonly int yPos;


      public int XPos {
         get { return xPos; }
      }

      public int YPos {
         get { return yPos; }
      }


      internal Location (int XPosition, int YPosition) {
         xPos = XPosition;
         yPos = YPosition;
      }


      public override string ToString ( ) {
         return string.Format("[{0}, {1}]", xPos, yPos);
      }
   }

   [Flags]
   public enum Direction {
      Unknown = 0,
      North = 1,
      South = 2,
      East = 4,
      West = 8,
      NorthEast = North | East,
      NorthWest = North | West,
      SouthEast = South | East,
      SouthWest = South | West,
   }
}
