﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Transport.Backend {
   internal class Engine {
      public int Speed { get; private set; }

      internal Engine (int speed) {
         Speed = speed;
      }
   }
}
