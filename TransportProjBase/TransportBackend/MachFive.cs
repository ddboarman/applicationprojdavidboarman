﻿using System;

namespace Transport.Backend {
   public class MachFive : Car {
      private Engine engine;

      public MachFive (int xPos, int yPos, City city, Passenger passenger)
         : base(xPos, yPos, city, "Mach5", passenger) {
         engine = new Engine(5);
         Speed = engine.Speed;
      }

      protected override void MoveUp ( ) {
         if (Location.YPos < City.Map.Width) {
            Location = new Location(Location.XPos, Location.YPos + 1);
         }
      }

      protected override void MoveDown ( ) {
         if (Location.YPos > 0) {
            Location = new Location(Location.XPos, Location.YPos - 1);
         }
      }

      protected override void MoveRight ( ) {
         if (Location.XPos < City.Map.Length) {
            Location = new Location(Location.XPos + 1, Location.YPos);
         }
      }

      protected override void MoveLeft ( ) {
         if (Location.XPos > 0) {
            Location = new Location(Location.XPos - 1, Location.YPos);
         }
      }
   }
}
