﻿using System;
using System.Collections.Generic;

using Transport.Domain;

namespace Transport.Backend {
   public class City {
      public List<Car> RegisteredCars { get; private set; }

      public Map Map { get; private set; }

      public City (int xMax, int yMax) {
         RegisteredCars = new List<Car>( );
         Map = new CityMap(xMax, yMax);
      }

      public Car AddCarToCity (Car car) {
         RegisteredCars.Add(car);

         return car;
      }

      public Passenger AddPassengerToCity (int startXPos, int startYPos, int destXPos, int destYPos) {
         Passenger passenger = new Passenger(startXPos, startYPos, destXPos, destYPos, this);

         return passenger;
      }

   }
}
