﻿using System;
using System.Linq;

namespace Transport.Backend {
   public delegate void CarMoved(Car Car, Direction Direction, Location OldLocation, Location NewLocation);

   public abstract class Car {

      private Location previous;
      private Direction orientation;

      public Location Location { get; protected set; }
      public Passenger Passenger { get; private set; }
      public City City { get; private set; }
      public string VIN { get; private set; }
      public int Speed { get; protected set; }
      public string ID { get; protected set; }
      public string BodyType { get; protected set; }
      
      public Car (int xPos, int yPos, City city, string bodyType, Passenger passenger) {
         Location = new Transport.Backend.Location(xPos, yPos);

         City = city;
         Passenger = passenger;
         BodyType = bodyType;

         // use guid for VIN
         VIN = Guid.NewGuid( ).ToString( ).ToUpper().Replace('-', '\0');
         // use the first 7 characters of VIN as short ID
         ID = new String(VIN.Take(7).ToArray( ));

         Navigator.CarMoved += OnCarMoved;
      }

      protected virtual void OnCarMoved (Car Car, Direction Direction, Location OldLocation, Location NewLocation) {
         if (Car.VIN == VIN) {
            previous = OldLocation;
            Location = NewLocation;
            orientation = Direction;
            WritePositionToConsole( );
         }
      }

      protected virtual void WritePositionToConsole ( ) {
         Console.WriteLine(String.Format("{0} moved {1} from:{2}   to:{3}", 
            BodyType,
            orientation,
            previous, 
            Location));
      }

      public void PickupPassenger (Passenger passenger) {
         Passenger = passenger;
         Console.WriteLine("Assignment: Pick up passenger at location [{0}, {1}]",
            passenger.StartingXPos,
            passenger.StartingYPos);
      }

      public virtual Direction Move (Direction direction) {
         switch (direction) {
            case Direction.North:
               MoveUp( );
               break;
            case Direction.South:
               MoveDown( );
               break;
            case Direction.East:
               MoveRight( );
               break;
            case Direction.West:
               MoveLeft( );
               break;
            case Direction.NorthEast:
               break;
            case Direction.NorthWest:
               break;
            case Direction.SouthEast:
               break;
            case Direction.SouthWest:
               break;
            case Direction.Unknown:
            default:
               break;
         }


         return direction;
      }

      protected abstract void MoveUp ( );

      protected abstract void MoveDown ( );

      protected abstract void MoveRight ( );

      protected abstract void MoveLeft ( );

      public void AssignDestination (Location location) {
         Console.WriteLine("Assignment: Deliver passenger to location {0}",
            location);
      }

      public void DropOffPassenger (Passenger passenger) {
         Passenger = null;
      }
   }
}
