﻿
namespace Transport.Backend {
   public class CityPosition {
      public City City { get; private set; }
      public Car Car { get; set; }
      public Passenger Passenger { get; set; }

      public CityPosition (City city) {
         City = city;
      }
   }
}
