﻿using System;

using Transport.Domain;

namespace Transport.Backend {
   class CityMap : Map {
      public int Length { get; private set; }
      public int Width { get; private set; }

      public CityMap (int XMax, int YMax) {
         // TODO: Complete member initialization
         Length = XMax;
         Width = YMax;
      }

   }
}
