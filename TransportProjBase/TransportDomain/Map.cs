﻿using System;

namespace Transport.Domain {
   public interface Map {

      int Length { get; }
      int Width { get; }

   }
}
