﻿using System;

namespace Transport.Domain {
   public delegate void MatrixPopulated (int Length, int Width);

   public interface IMapMatrix {
      event MatrixPopulated MatrixPopulated;

   }
}
