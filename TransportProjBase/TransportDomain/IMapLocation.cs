﻿using System;

namespace Transport.Domain {
   public interface IMapLocation {
      int XPos { get; }
      int YPos { get; }
   }
}
